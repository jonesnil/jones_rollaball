﻿using UnityEngine;
using UnityEngine.UI;

//I just load this so I can reset the scene later if someone falls out of the map.
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerBehavior : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;

    // At first I thought the cameraHelper would just reference this and not the other way
    // around. However, when I was trying to make the player move forward I realized it would
    // need to know how the camera is rotated. Here it imports the cameraHelper to get that info
    // later for movement.
    public Transform cameraHelper = null;

    // The hitCannon variable is there to give information about the cannon we touch, 
    // so it can figure out where it needs to point the camera based on the cannon info.
    // The variable is only set when the player touches a cannon, and is reset if it touches 
    // a different one. The boolean is just there to tell the program if it should be updating 
    // the camera anymore or not. It of course is set to true only when the player touches a cannon.
    public Transform hitCannon = null;
    public bool shot = false;

    private Rigidbody rb;
    private int count;

    void Start()
    {
        cameraHelper = GameObject.Find("CameraHelper").transform;

        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        speed = 10;
    }

    void FixedUpdate()
    {

        // The last two features took a couple hours each to implement. I decided for the last one
        // to do something that wouldn't require googling 15 different things, but would still make
        // the game a lot more playable. The cannons made it possible to fall out of the map if you
        // mess up your trajectory with the keys while in the air- this code makes it so if you do 
        // fall out the scene loads fresh so you can keep playing.
        if (transform.position.y < -10) {
            SceneManager.LoadScene("Play Area");
        }

        // It only goes into this loop if it touches a cannon, and this loop is for fixing the camera
        // to face where the player was shot.
        
        if (shot){

            // These if statements are the two things that will stop the program from updating the camera.
            // The first one says it should stop if the camera angle equals what it should equal after the 
            // calculations. The second one says it should stop if the player tries to move the camera, in
            // case they don't want to face that way.

            if (cameraHelper.rotation.eulerAngles == Quaternion.Euler(0f, hitCannon.rotation.eulerAngles.y - 120f, 0f).eulerAngles) {
                shot = false;
            }

            if (Input.GetKey("a") || Input.GetKey("d") || Input.GetKey("left") || Input.GetKey("right"))
            {
                shot = false;
            }

            // This math took me a while to figure out. I finally found that I can use the y value
            // of the cannon to determine which direction it's facing, but it doesnt map directly. 
            // I experimented with different values to make it face right every time, and discovered
            // if I subtract 120 from the y value it faces it will point about where I want it to.

            // Also, it's probably not super clear, the camera only rotates on the y axis ever, thats why
            // I take only the cannon's y  value. If I took the whole rotation here the camera would be 
            // completely messed up for the rest of the game, I only want it to point the way it would 
            // point naturally if the player looked themself.

            Quaternion rotation = cameraHelper.rotation;
            Quaternion desiredRotation = Quaternion.Euler(0f, hitCannon.rotation.eulerAngles.y - 120f, 0f);

            // This is just the trick I learned from class to make the movement look smooth. It slowly brings
            // the camera rotation to the one I actually want, that faces the shot from the cannon.
            cameraHelper.rotation = Quaternion.Lerp(rotation, desiredRotation, 0.1f);
        }


        /* I got confused trying to use the Input.GetAxis method for this and I eventually
         * decided to just check the keys directly instead. The if statements are to act 
         * differently depending on if the player is moving forward or backward, and the 
         * cameraHelper.forward is used as a base to make sure the ball movement is intuitive
         * based on how the camera is looking at the player. 
         * 
         * The reason it uses cameraHelper.forward and not its own transform.forward is because it
         * being a ball makes its rotation crazy, I tried that method first and the ball would sometimes
         * fly into the air depending on the rotation. Conveniently, the cameraHelper already does the 
         * rotating math, so I can just throw that in.
         */

        if (Input.GetKey("up") || Input.GetKey("w"))
        {
            rb.AddForce(cameraHelper.forward * speed);
        }
        if (Input.GetKey("down") || Input.GetKey("s"))
        {
            rb.AddForce(cameraHelper.forward * -speed);
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }

        // I set the cannon to let the player collide with it, and gave it the
        // cannon tag so the player can realize when it hits one.
        if (other.gameObject.CompareTag("Cannon"))
        {
            //this is just the speed the cannon will shoot the player, probably obvious.
            float launchSpeed = 1250f;

            // This step puts the rigidbody back to completely still. It took me a minute to realize
            // I needed this step, but without it the forces added by the player before it gets here
            // will affect the cannon's shot. This way, it shoots the player the same way every time.

            //(Though the player can adjust their trajectory in midair using the keyboard. I consider
            // that a feature rather than a bug, I don't care if the player wants to change their trajectory.)

            rb.velocity = new Vector3 (0f, 0f, 0f);

            // This also took me a second. Using transform up was somewhat intuitive, because I of course
            // want the player to be fired as if from the top of the capsule shape. I think normalizing the
            // Vector3 makes it shoot more how you would expect though, and makes the speed more consistent
            // compared to how far it launches. 
            Vector3 launchTrajectory = other.gameObject.transform.up.normalized * launchSpeed;
            rb.AddForce(launchTrajectory);

            // Like I went over earlier, this just makes sure the program knows later it's time to update
            // the camera. It also keeps track of the cannon the player just hit, so the program can tell
            // where to point.
            shot = true;
            hitCannon = other.transform;
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 14)
        {
            winText.text = "You Win!";
        }
    }
}