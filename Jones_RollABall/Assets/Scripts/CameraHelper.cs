﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This node exists to solve a weird problem I ran into. I wanted to make the camera
 * a child under the player class so I could easily make the camera rotate around the 
 * player- but, the player is a ball, meaning if it's a child of it it will rotate
 * uncontrollably. 
 * 
 * So I made this empty node, which constantly keeps itself at the same position as the ball,
 * without rotating like crazy, and made it the parent of the camera. Kind of janky but it 
 * was the first thing I thought to try.
 * */

public class CameraHelper : MonoBehaviour
{

    // This is the speed the camera will rotate around the player
    float speed = 1.5f;

    //This is where I'll keep track of the player (the ball.)
    Transform player = null;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        // This line is what I made this for in the first place- it makes the position of this
        // node always the same as the player, so I can rotate the camera around this as if it
        // were the player (without irritating spinning.)
        this.transform.position = player.position;

        // Took me a while to come up with this. It just takes the left and right input and rotates
        // this node based on it. This also rotates its child, the camera, and because this is at the player
        // position it rotates around the player.
        float rotateHorizontal = Input.GetAxis("Horizontal");
        transform.Rotate(0f,rotateHorizontal*speed, 0f, Space.Self);
    }
}
